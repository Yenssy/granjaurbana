// crear un objeto (variable compuestas) para representar el visor
//L representa a la biblioteca leaflet
let miMapa = L.map('mapid');

//Determinar la vista inicial
miMapa.setView([4.58009652670252, -74.10823781043291], 16);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
miProveedor.addTo(miMapa);


  


//Crear un objeto marcador
let miMarcador = L.marker([4.58009652670252, -74.10823781043291]);
miMarcador.addTo(miMapa);

miMarcador.bindPopup("<b>Granja Urbana de Yen:).</b> <br> Planta: fríjol. <br> Fecha plantación: 14/01/2021").openPopup();


let miGeoJSON= L.geoJSON(sitio);

miGeoJSON.addTo(miMapa);

let miTitulo=document.getElementById("titulo");
miTitulo.textContent=sitio.features[0].properties.popupContent;

let miDescripcion=document.getElementById("descripcion");
miDescripcion.textContent=sitio.features[0].properties.description;
